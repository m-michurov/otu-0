s = %s;

k = 4.0

G = k/s;
sys = syslin('c', G);
f_min = .0001; f_max = 16;

clf(); 
bode(sys, f_min, f_max, "rad"); // Converts Hz to rad/s
